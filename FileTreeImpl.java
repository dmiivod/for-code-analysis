package com.efimchick.ifmo.io.filetree;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class FileTreeImpl implements FileTree {
    StringBuilder sb = new StringBuilder();

    @Override
    public Optional<String> tree(Path path) {
        if (path == null || !path.toFile().exists()) {
            return Optional.empty();
        }
        if(path.toFile().isFile()) {
            return Optional.of(path.toFile().getName() + " " + path.toFile().length() + " bytes");
        } else {
            StringBuilder result;
            try {
                result = new StringBuilder(dirTree(0,path));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return Optional.of(result.toString());
        }

    }


     private StringBuilder dirTree(int folderDepth, Path path) throws IOException {

        if (folderDepth == 0) {
            sb.append(path.toFile().getName()).append(" ")
                    .append(getSize(path.toFile())).append(" bytes").append("\n");
        }

        List<File> files = new LinkedList<>();
        Files.walk(path, 1)
                .filter(x -> x.toFile().isDirectory())
                .sorted()
                .forEach(x -> files.add(x.toFile()));

        files.remove(0);

        Files.walk(path, 1)
                .filter(x -> x.toFile().isFile())
                .sorted()
                .forEach(x -> files.add(x.toFile()));



        for(int i = 0; i < files.size(); i++) {
            if ((i == files.size() -1) && files.get(i).isDirectory() ) {
                sb.append(buildLines(folderPosition(folderDepth, files.get(i).toPath())));
                sb.append("└─ ").append(files.get(i).getName()).append(" ")
                        .append(getSize(files.get(i))).append(" bytes").append("\n");

                dirTree(folderDepth + 1, files.get(i).toPath());
                continue;
            } else if ((i == files.size() -1) && files.get(i).isFile()) {
                sb.append(buildLines(folderPosition(folderDepth, files.get(i).toPath())));
                sb.append("└─ ").append(files.get(i).getName()).append(" ")
                        .append(getSize(files.get(i))).append(" bytes").append("\n");
                continue;
            }
            if (files.get(i).isDirectory()) {
                sb.append(buildLines(folderPosition(folderDepth, files.get(i).toPath())));
                sb.append("├─ ").append(files.get(i).getName()).append(" ")
                        .append(getSize(files.get(i))).append(" bytes").append("\n");

                dirTree(folderDepth + 1, files.get(i).toPath());
            } else {
                sb.append(buildLines(folderPosition(folderDepth, files.get(i).toPath())));
                sb.append("├─ ").append(files.get(i).getName()).append(" ")
                        .append(getSize(files.get(i))).append(" bytes").append("\n");
            }
        }
        return sb;
    }

    private List<Boolean> folderPosition(int depth, Path path) throws IOException {
        Path child = path.getParent();
        Path parent = child.getParent();
        List<Boolean> result = new LinkedList<>();

        for (int i = 0; i < depth; i++){
            int indexOfChild = listOfFiles(parent).indexOf(child.toFile().getName());
            if(indexOfChild == listOfFiles(parent).size() - 1) {
                result.add(0,true);
            } else {
                result.add(0,false);
            }
            child = parent;
            parent = child.getParent();
        }
        return result;
    }

    private List<String> listOfFiles (Path path) throws IOException {
        List<String> files = new LinkedList<>();
        Files.walk(path, 1)
                .filter(x -> x.toFile().isDirectory())
                .forEach(x -> files.add(x.toFile().getName()));
        files.remove(0);
        Files.walk(path, 1)
                .filter(x -> x.toFile().isFile())
                .forEach(x -> files.add(x.toFile().getName()));
        return files;
    }

    private StringBuilder buildLines (List<Boolean> list) {
        StringBuilder sb = new StringBuilder();
        for(Boolean result : list) {
            if (result) {
                sb.append("   ");
            }else{
                sb.append("│  ");
            }
        }
        return sb;
    }

    private long getSize(File file) {
        long length = 0;
        if (file.isFile()) {
            return file.length();
        } else {
            File[] files = file.listFiles();

            for (File value : files) {
                if (value.isFile()) {
                    length += value.length();
                } else {
                    length += getSize(value);
                }
            }
            return length;
        }
    }
}
